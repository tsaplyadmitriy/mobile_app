import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '/elements/elements.dart';
import '/profile.dart';
import '/suggestions.dart';
import '/swipe_cards.dart';
import 'authentication/login.dart';
import 'authentication/onboading.dart';

class NavigationTabController extends StatefulWidget {
  const NavigationTabController({Key? key}) : super(key: key);

  @override
  _NavigationTabControllerState createState() =>
      _NavigationTabControllerState();
}

class _NavigationTabControllerState extends State<NavigationTabController> {
  late PersistentTabController _controller;

  @override
  void initState() {
    super.initState();
    _controller = PersistentTabController(initialIndex: 0);
  }

  List<Widget> _buildScreens() {
    return [
      const SuggestionsScreen(),
      const SwipeCardsScreen(),
      const ProfileScreen(),
      // TestRoutes(title: "Routes"),
    ];
  }

  List<PersistentBottomNavBarItem> _navBarsItems() {
    return [
      PersistentBottomNavBarItem(
        icon: const Icon(Icons.reorder),
        title: 'Collection',
        activeColorPrimary: CupertinoColors.systemYellow,
        inactiveColorPrimary: CupertinoColors.inactiveGray,
      ),
      PersistentBottomNavBarItem(
        icon: const Icon(Icons.favorite),
        title: ('Like'),
        activeColorPrimary: CupertinoColors.systemTeal,
        inactiveColorPrimary: CupertinoColors.inactiveGray,
      ),
      PersistentBottomNavBarItem(
        icon: const Icon(Icons.people),
        title: ('Profile'),
        activeColorPrimary: CupertinoColors.systemRed,
        inactiveColorPrimary: CupertinoColors.inactiveGray,
      ),
      // PersistentBottomNavBarItem(
      //     icon: Icon(Icons.dangerous_rounded),
      //     title: ("Debug"),
      //     activeColorPrimary: CupertinoColors.systemRed,
      //     inactiveColorPrimary: CupertinoColors.inactiveGray,
      //     routeAndNavigatorSettings: RouteAndNavigatorSettings(routes: {
      //       '/': (context) => NavigationTabController(),
      //       // '/': (context) => MyHomePage(title: ""),
      //       '/onboarding': (context) => OnboardingScreen(),
      //       '/login': (context) => LoginScreen(),
      //       '/signup': (context) => SignupScreen(),
      //       '/profile': (context) => ProfileScreen(),
      //       '/swipe': (context) => SwipeCardsScreen(),
      //     })),
    ];
  }

  @override
  Widget build(BuildContext context) {
    final darkActive =
        MediaQuery.of(context).platformBrightness == Brightness.dark;
    return FutureBuilder<AuthStates>(
        future: getAuthState(),
        builder: (BuildContext context, AsyncSnapshot<AuthStates> snapshot) {
          if (!snapshot.hasData) {
            return const SplashLoadingScreen();
          } else if (snapshot.hasData) {
            if (snapshot.data == AuthStates.firstBoot) {
              return const OnboardingScreen();
            } else if (snapshot.data == AuthStates.loggedOut) {
              return const LoginScreen();
            } else if (snapshot.data == AuthStates.loggedIn) {
              return PersistentTabView(
                context,
                controller: _controller,
                screens: _buildScreens(),
                items: _navBarsItems(),
                confineInSafeArea: true,
                backgroundColor: !darkActive
                    ? Colors.white
                    : Theme.of(context).canvasColor, // Default is Colors.white.
                handleAndroidBackButtonPress: true, // Default is true.
                resizeToAvoidBottomInset:
                    true, // This needs to be true if you want to move up the screen when keyboard appears. Default is true.
                stateManagement: true, // Default is true.
                hideNavigationBarWhenKeyboardShows:
                    true, // Recommended to set 'resizeToAvoidBottomInset' as true while using this argument. Default is true.
                decoration: const NavBarDecoration(
                  // boxShadow: [
                  //   BoxShadow(
                  //       offset: Offset(0, -1),
                  //       blurRadius: 10.0,
                  //       spreadRadius: 5,
                  //       color: darkActive
                  //           ? CupertinoColors.black
                  //           : Colors.grey.shade400)
                  // ],
                  // borderRadius: BorderRadius.circular(20.0),
                  colorBehindNavBar: Colors.white,
                ),
                popAllScreensOnTapOfSelectedTab: true,
                popActionScreens: PopActionScreensType.all,
                itemAnimationProperties: const ItemAnimationProperties(
                  // Navigation Bar's items animation properties.
                  duration: Duration(milliseconds: 200),
                  curve: Curves.ease,
                ),
                screenTransitionAnimation: const ScreenTransitionAnimation(
                  // Screen transition animation on change of selected tab.
                  animateTabTransition: true,
                  curve: Curves.ease,
                  duration: Duration(milliseconds: 200),
                ),
                navBarStyle: NavBarStyle
                    .style12, // Choose the nav bar style with this property.
              );
            }
          }
          throw Exception('snaphot broken');
        });
  }

  Future<AuthStates> getAuthState() async {
    final prefs = await SharedPreferences.getInstance();
    if (prefs.containsKey('authState')) {
      final authState = prefs.getString('authState');
      if (authState == 'loggedIn') {
        return AuthStates.loggedIn;
      } else if (authState == 'loggedOut') {
        return AuthStates.loggedOut;
      }
      throw Exception('Unknown login state');
    }
    return AuthStates.firstBoot;
  }
}

class SplashLoadingScreen extends StatelessWidget {
  const SplashLoadingScreen({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          swipioLogo(),
          const SizedBox(height: 40),
          const CircularProgressIndicator(),
        ],
      ),
    ));
  }
}

enum AuthStates { firstBoot, loggedIn, loggedOut }
