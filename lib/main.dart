import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '/elements/elements.dart';
import '/locator.dart';
import '/profile.dart';
import '/swipe_cards.dart';
import '/tab_controller.dart';
import 'authentication/login.dart';
import 'authentication/onboading.dart';
import 'authentication/signup.dart';

void main() {
  initLocator();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Swipio',
      theme: ThemeData(
        brightness: Brightness.light,
        primarySwatch: Colors.cyan,
        elevatedButtonTheme: elevatedButtonStyle,
      ),
      darkTheme: ThemeData(
        brightness: Brightness.dark,
        primarySwatch: Colors.teal,
        elevatedButtonTheme: elevatedButtonStyle,
      ),
      routes: {
        '/': (context) => const NavigationTabController(),
        '/onboarding': (context) => const OnboardingScreen(),
        '/login': (context) => const LoginScreen(),
        '/signup': (context) => const SignupScreen(),
        '/profile': (context) => const ProfileScreen(),
        '/swipe': (context) => const SwipeCardsScreen(),
      },
    );
  }
}
