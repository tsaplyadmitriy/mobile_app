import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '/bloc/auth/auth_bloc.dart';
import '/elements/elements.dart';
import '/locator.dart';
import '/network/models/registration_response.dart';
import '/network/repository/auth_repository.dart';

class SignupScreen extends StatefulWidget {
  const SignupScreen({Key? key}) : super(key: key);

  @override
  _SignupScreenState createState() => _SignupScreenState();
}

class _SignupScreenState extends State<SignupScreen> {
  late TextEditingController firstNameController;
  late TextEditingController lastNameController;
  late TextEditingController emailController;
  late TextEditingController passwordController1;
  late TextEditingController passwordController2;
  late FocusNode emailFocus;
  late FocusNode firstNameFocus;
  late FocusNode lastNameFocus;
  late FocusNode passwordFocus1;
  late FocusNode passwordFocus2;
  bool isChecked = false;

  final _emailKey = GlobalKey<FormState>();
  final _firstNameFormKey = GlobalKey<FormState>();
  final _lastNameFormKey = GlobalKey<FormState>();
  final _passwordKey = GlobalKey<FormState>();

  @override
  initState() {
    firstNameController = TextEditingController(text: 'test');
    emailController = TextEditingController(text: 'testemail@mail.ru');
    lastNameController = TextEditingController(text: 'test');
    passwordController1 = TextEditingController(text: 'password');
    passwordController2 = TextEditingController(text: 'password');
    firstNameFocus = FocusNode();
    lastNameFocus = FocusNode();
    emailFocus = FocusNode();
    passwordFocus1 = FocusNode();
    passwordFocus2 = FocusNode();

    super.initState();
  }

  @override
  void dispose() {
    emailFocus.dispose();
    passwordFocus1.dispose();
    passwordFocus2.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Padding(
      padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 20),
      child: BlocProvider(
        create: (_) => AuthBloc(locator<AuthenticationRepository>()),
        child:
            BlocConsumer<AuthBloc, AuthState>(listener: (context, state) async {
          if (state is RegistrationFailure) {
            if (state.status == RegistrationStatus.emailTaken) {
              emailController.text = '';
              swipioToast(
                  context, 'Username is taken', Icons.error_outline_rounded);
            }
          }
          if (state is RegistrationSuccess) {
            swipioToast(context, 'Logging you in...', Icons.check_circle);
            Navigator.of(context).pushReplacementNamed('/');
          }
        }, builder: (context, state) {
          return Stack(children: [
            state is LoadingState
                ? const Center(child: CircularProgressIndicator())
                : SingleChildScrollView(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        // const Spacer(flex: 2),
                        const SizedBox(height: 40),
                        swipioLogo(),
                        const Text('Sign Up',
                            style: TextStyle(
                                fontSize: 30, fontWeight: FontWeight.bold)),
                        // const Spacer(flex: 3),
                        const SizedBox(height: 16),
                        Form(
                          key: _firstNameFormKey,
                          child: TextFormField(
                            autocorrect: false,
                            keyboardType: TextInputType.name,
                            controller: firstNameController,
                            focusNode: firstNameFocus,
                            validator: (value) {
                              return RegExp(r'\w+').hasMatch(value ?? '')
                                  ? null
                                  : 'Incorrect format';
                            },
                            decoration: InputDecoration(
                              labelText: 'first name',
                              border: InputBorder.none,
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(5),
                                borderSide: const BorderSide(
                                  color: Colors.grey,
                                  width: 1.0,
                                ),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30),
                                borderSide: BorderSide(
                                  color:
                                      Theme.of(context).colorScheme.secondary,
                                  width: 1.5,
                                ),
                              ),
                            ),
                            obscureText: false,
                            textInputAction: TextInputAction.next,
                          ),
                        ),
                        const SizedBox(height: 16),
                        Form(
                          key: _lastNameFormKey,
                          child: TextFormField(
                            autocorrect: false,
                            keyboardType: TextInputType.name,
                            controller: lastNameController,
                            focusNode: lastNameFocus,
                            validator: (value) {
                              return RegExp(r'\w+').hasMatch(value ?? '')
                                  ? null
                                  : 'Incorrect format';
                            },
                            decoration: InputDecoration(
                              labelText: 'last name',
                              border: InputBorder.none,
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(5),
                                borderSide: const BorderSide(
                                  color: Colors.grey,
                                  width: 1.0,
                                ),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30),
                                borderSide: BorderSide(
                                  color:
                                      Theme.of(context).colorScheme.secondary,
                                  width: 1.5,
                                ),
                              ),
                            ),
                            obscureText: false,
                            textInputAction: TextInputAction.next,
                          ),
                        ),
                        const SizedBox(height: 16),
                        Form(
                          key: _emailKey,
                          child: TextFormField(
                            autocorrect: false,
                            keyboardType: TextInputType.emailAddress,
                            controller: emailController,
                            focusNode: emailFocus,
                            textInputAction: TextInputAction.next,
                            validator: (value) {
                              return RegExp(
                                          r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                                      .hasMatch(value ?? '')
                                  ? null
                                  : 'Bad email format';
                            },
                            decoration: InputDecoration(
                              labelText: 'email',
                              border: InputBorder.none,
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(5),
                                borderSide: const BorderSide(
                                  color: Colors.grey,
                                  width: 1.0,
                                ),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30),
                                borderSide: BorderSide(
                                  color:
                                      Theme.of(context).colorScheme.secondary,
                                  width: 1.5,
                                ),
                              ),
                            ),
                          ),
                        ),
                        const SizedBox(height: 16),
                        TextFormField(
                          autocorrect: false,
                          controller: passwordController1,
                          focusNode: passwordFocus1,
                          decoration: InputDecoration(
                            labelText: 'password',
                            border: InputBorder.none,
                            enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(5),
                              borderSide: const BorderSide(
                                color: Colors.grey,
                                width: 1.0,
                              ),
                            ),
                            focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(30),
                              borderSide: BorderSide(
                                color: Theme.of(context).colorScheme.secondary,
                                width: 1.5,
                              ),
                            ),
                          ),
                          obscureText: true,
                          textInputAction: TextInputAction.next,
                        ),
                        const SizedBox(height: 16),
                        Form(
                          key: _passwordKey,
                          child: TextFormField(
                            autocorrect: false,
                            controller: passwordController2,
                            focusNode: passwordFocus2,
                            validator: (value) {
                              return passwordController1.text == value &&
                                      passwordController1.text.isNotEmpty
                                  ? null
                                  : 'Password is empty or does not match';
                            },
                            decoration: InputDecoration(
                              labelText: 'confirm password',
                              border: InputBorder.none,
                              enabledBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(5),
                                borderSide: const BorderSide(
                                  color: Colors.grey,
                                  width: 1.0,
                                ),
                              ),
                              focusedBorder: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(30),
                                borderSide: BorderSide(
                                  color:
                                      Theme.of(context).colorScheme.secondary,
                                  width: 1.5,
                                ),
                              ),
                            ),
                            obscureText: true,
                            textInputAction: TextInputAction.done,
                          ),
                        ),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Checkbox(
                              value: isChecked,
                              onChanged: (bool? value) {
                                setState(() {
                                  isChecked = value!;
                                });
                              },
                            ),
                            const Text('I agree with Terms and Conditions')
                          ],
                        ),
                        // const Spacer(flex: 4),
                        ElevatedButton(
                          onPressed: () {
                            if (_firstNameFormKey.currentState!.validate() &&
                                _lastNameFormKey.currentState!.validate() &&
                                _emailKey.currentState!.validate() &&
                                _passwordKey.currentState!.validate() &&
                                isChecked) {
                              BlocProvider.of<AuthBloc>(context).add(
                                  RegisterUserEvent(
                                      emailController.text,
                                      passwordController1.text,
                                      firstNameController.text,
                                      lastNameController.text));
                            }
                          },
                          child: const Padding(
                            padding: EdgeInsets.symmetric(horizontal: 40),
                            child: Text('Sign up'),
                          ),
                        ),
                        TextButton(
                          child: const Text('Sign In'),
                          onPressed: () {
                            Navigator.of(context)
                                .pushReplacementNamed('/login');
                          },
                        ),
                        // const Spacer(flex: 1),
                      ],
                    ),
                  ),
          ]);
        }),
      ),
    ));
  }
}
