import 'package:dio/dio.dart';
import 'package:retrofit/retrofit.dart';

import '/network/base_url.dart';
import '/network/models/film_models.dart';
import '/network/models/group_model.dart';

part 'group_api.g.dart';

@RestApi(baseUrl: BaseUrls.baseUrl)
abstract class GroupApi {
  factory GroupApi(Dio dio) = _GroupApi;

  @POST('/api/v1/groups/create')
  Future<GroupResponse> createGroup(@Header('Authorization') String? token,
      @Query('name') String name, @Query('description') String description);

  @GET('/api/v1/groups/{group_id}')
  Future<GroupResponse> getGroup(
      @Header('Authorization') String? token, @Path('group_id') int groupId);

  @POST('/api/v1/groups/{group_id}/join')
  Future<String> joinGroup(
      @Header('Authorization') String? token, @Path('group_id') int groupId);

  @GET('/api/v1/groups/{group_id}/top_films')
  Future<FilmsResponse> topFilms(
      @Header('Authorization') String? token, @Path('group_id') int groupId);

  @GET('/api/v1/groups/{group_id}/films_to_view')
  Future<FilmsResponse> filmsToView(@Header('Authorization') String? token,
      @Path('group_id') int groupId, @Query('count') int count);

  @PUT('/api/v1/film_marks/{film_id}/edit')
  Future<String> markFilm(@Header('Authorization') String? token,
      @Path('film_id') int filmId, @Query('mark') String mark);
}
