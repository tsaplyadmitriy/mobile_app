import 'package:json_annotation/json_annotation.dart';

import '/network/models/user_model.dart';

part 'group_model.g.dart';

@JsonSerializable()
class GroupResponse {
  final GroupModel group;

  GroupResponse(this.group);

  factory GroupResponse.fromJson(Map<String, dynamic> json) =>
      _$GroupResponseFromJson(json);

  Map<String, dynamic> toJson() => _$GroupResponseToJson(this);
}

@JsonSerializable()
class GroupModel {
  @JsonKey(name: 'created_at')
  final String created;
  @JsonKey(name: 'updated_at')
  final String updated;
  @JsonKey(name: 'group_id')
  final int groupId;
  final String name;
  final String description;
  final List<UserModelContainer> users;

  GroupModel(this.created, this.updated, this.groupId, this.name,
      this.description, this.users);

  factory GroupModel.fromJson(Map<String, dynamic> json) =>
      _$GroupModelFromJson(json);

  Map<String, dynamic> toJson() => _$GroupModelToJson(this);
}
