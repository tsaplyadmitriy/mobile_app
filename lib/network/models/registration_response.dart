import 'package:json_annotation/json_annotation.dart';

import '/network/models/user_model.dart';

part 'registration_response.g.dart';

@JsonSerializable()
class RegistrationResponse {
  final UserModel user;
  RegistrationStatus state;

  RegistrationResponse(this.user, {this.state = RegistrationStatus.success});

  factory RegistrationResponse.fromJson(Map<String, dynamic> json) =>
      _$RegistrationResponseFromJson(json);
  Map<String, dynamic> toJson() => _$RegistrationResponseToJson(this);
}

enum RegistrationStatus { success, emailTaken }
