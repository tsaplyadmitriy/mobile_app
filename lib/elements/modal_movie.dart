import 'package:flutter/material.dart';

import '/suggestions.dart';
import '../network/models/film_models.dart';

class MovieModal extends StatefulWidget {
  final FilmContainer film;
  const MovieModal({Key? key, required this.film}) : super(key: key);

  @override
  _MovieModalState createState() => _MovieModalState();
}

class _MovieModalState extends State<MovieModal> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(centerTitle: true, title: Text(widget.film.film.title)),
      body: Container(
        padding: const EdgeInsets.symmetric(horizontal: 32, vertical: 8),
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Hero(
                tag: widget.film.film.filmId,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(16.0),
                  child: Image.network(widget.film.film.image,
                      fit: BoxFit.fitWidth),
                ),
              ),
              const SizedBox(height: 30),
              Text(widget.film.film.description,
                  style: const TextStyle(fontSize: 18)),
              const SizedBox(height: 30),
              RatingBar(
                filmContainer: widget.film,
              )
            ],
          ),
        ),
      ),
    );
  }
}
