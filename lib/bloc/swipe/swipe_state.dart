part of 'swipe_bloc.dart';

abstract class SwipeState {}

class InitialState extends SwipeState {}

class LoadingState extends SwipeState {}

class NoGroupState extends SwipeState {}

class UpdatedSwipe extends SwipeState {
  final Map<int, FilmsForGroup> swipeFilms;

  UpdatedSwipe(this.swipeFilms);
}
