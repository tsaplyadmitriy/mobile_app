part of 'swipe_bloc.dart';

abstract class SwipeEvent {}

class GetStatus extends SwipeEvent {}

class GetSwipes extends SwipeEvent {
  final List<int> groupIds;

  GetSwipes(this.groupIds);
}

class GetMoreSwipes extends SwipeEvent {
  final Map<int, FilmsForGroup> swipeFilms;
  final int updateId;

  GetMoreSwipes(this.swipeFilms, this.updateId);
}

enum SwipeType { left, right }

class SwipeAction extends SwipeEvent {
  final int filmId;
  final SwipeType type;
  final int groupId;
  final Map<int, FilmsForGroup> swipeFilmsBeforeSwipe;

  SwipeAction(this.filmId, this.type, this.groupId, this.swipeFilmsBeforeSwipe);
}
