part of 'film_suggestion_bloc.dart';

abstract class FilmSuggestionEvent {}

class GetStatus extends FilmSuggestionEvent {}

class GetFilmsToWatchEvent extends FilmSuggestionEvent {
  final List<int> groupIds;

  GetFilmsToWatchEvent(this.groupIds);
}
