part of 'group_bloc.dart';

abstract class GroupState {}

class InitialState extends GroupState {}

class LoadingState extends GroupState {}

class NoGroupState extends GroupState {}

class InGroupState extends GroupState {
  final List<GroupResponse> groups;

  InGroupState(this.groups);
}
