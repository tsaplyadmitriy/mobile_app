part of 'group_bloc.dart';

abstract class GroupEvent {}

class GetStatus extends GroupEvent {}

class CreateGroup extends GroupEvent {
  final String name;
  final String description;

  CreateGroup(this.name, this.description);
}

class GetGroups extends GroupEvent {
  final List<int> groupIds;

  GetGroups(this.groupIds);
}

class JoinGroup extends GroupEvent {
  final int groupId;

  JoinGroup(this.groupId);
}
